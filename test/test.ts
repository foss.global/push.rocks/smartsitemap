import { expect, tap } from '@pushrocks/tapbundle';
import * as smartsitemap from '../ts/index.js';

let testSmartsitemap: smartsitemap.SmartSitemap;

tap.test('should create an instance of Smartsitemap', async () => {
  testSmartsitemap = new smartsitemap.SmartSitemap();
  expect(testSmartsitemap).toBeInstanceOf(smartsitemap.SmartSitemap);
});

tap.test('should create a sitemap from feed', async () => {
  const sitemapString = await testSmartsitemap.createSitemapNewsFromFeedUrl('https://central.eu/feed');
  console.log(sitemapString);
});

tap.test('should parse a sitemap', async () => {
  const result = await testSmartsitemap.parseSitemapUrl('https://central.eu/sitemap-news');
  console.log(result.urlset.url);
})

tap.start();
