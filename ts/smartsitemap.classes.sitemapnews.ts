import * as plugins from './smartsitemap.plugins.js';
import * as interfaces from './interfaces/index.js';

export class SitemapNews {
  public rssItems: interfaces.IRssItem[] = [];

  constructor(optionsArg: {}) {}

  public async readAndAddFromRssFeedString(feedStringArg: string) {
    const smartfeedInstance = new plugins.smartfeed.Smartfeed();
    const parsedFeed = await smartfeedInstance.parseFeedFromString(feedStringArg);
    this.rssItems = this.rssItems.concat(parsedFeed.items);
  }

  public async readAndAddFromRssFeedUrl(urlArg: string) {
    const smartfeedInstance = new plugins.smartfeed.Smartfeed();
    const parsedFeed = await smartfeedInstance.parseFeedFromUrl(urlArg);
    this.rssItems = this.rssItems.concat(parsedFeed.items);
  }

  public async readAndParseArticles(articleArrayArg: plugins.tsclass.content.IArticle[]) {
    const rssItemArray = articleArrayArg.map((articleArg): interfaces.IRssItem => {
      return {
        title: articleArg.title,
        content: articleArg.content,
        isoDate: new Date(/* TODO: put article timestamp here */).toISOString(),
        link: articleArg.url
      };
    });

    this.rssItems = this.rssItems.concat(rssItemArray);
  }

  public exportSitemapXml() {
    const urls: {
      loc: string;
      'news:news': {
        'news:publication': {
          'news:name': string;
          'news:language': string;
        };
        'news:publication_date': string;
        'news:keywords': string;
        'news:title': string;
      };
    }[] = [];
    for (const itemArg of this.rssItems) {
      console.log(itemArg);
      urls.push({
        loc: itemArg.link,
        'news:news': {
          'news:publication': {
            'news:language': 'en',
            'news:name': 'some name',
          },
          'news:keywords': '',
          'news:publication_date': itemArg.isoDate,
          'news:title': itemArg.title,
        },
      });
    }
    const sitemapObject: any = {
      urlset: {
        '@_xmlns': 'http://www.sitemaps.org/schemas/sitemap/0.9',
        '@_xmlns:news': 'http://www.google.com/schemas/sitemap-news/0.9',
        url: urls,
      },
    };
    const smartxmlInstance = new plugins.smartxml.SmartXml();
    const sitemapString = smartxmlInstance.createXmlFromObject(sitemapObject);
    return sitemapString;
  }
}
