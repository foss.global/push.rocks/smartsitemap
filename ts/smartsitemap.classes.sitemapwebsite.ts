import * as plugins from './smartsitemap.plugins.js';

export type TUpdateFrequency = 'never' | 'daily' | 'weekly' | 'monthly' | 'yearly';

export interface IUrlInfo {
  url: string;
  timestamp: number;
  frequency?: TUpdateFrequency;
}

export class SitemapWebsite {
  urlInfos: IUrlInfo[] = [];
  constructor() {}

  public addUrl(urlInfoArg: IUrlInfo) {
    this.urlInfos.push(urlInfoArg);
  }

  public exportSitemapXml() {
    const urls: {
      loc: string;
      lastmod: string;
      changefreq: TUpdateFrequency;
    }[] = [];
    for (const urlInfoArg of this.urlInfos) {
      urls.push({
        loc: urlInfoArg.url,
        lastmod: new Date(urlInfoArg.timestamp).toISOString(),
        changefreq: urlInfoArg.frequency ? urlInfoArg.frequency : 'weekly'
      });
    }
    const sitemapObject: any = {
      urlset: {
        '@_xmlns': 'http://www.sitemaps.org/schemas/sitemap/0.9',
        url: urls,
      },
    };
    const smartxmlInstance = new plugins.smartxml.SmartXml();
    const sitemapString = smartxmlInstance.createXmlFromObject(sitemapObject);
    return sitemapString;
  }
}