export interface ISitemapYaml {
  daily: string[];
}

export interface IRssItem {
  [key: string]: any;
  link?: string;
  guid?: string;
  title?: string;
  pubDate?: string;
  creator?: string;
  content?: string;
  isoDate?: string;
  categories?: string[];
  contentSnippet?: string;
  enclosure?: any;
}

export interface IParsedSiteMap {
  urlset: {
    url:
      | {
          loc: string;
          lastmod: string;
          changefreq: string;
        }
      | {
          loc: string;
          lastmod: string;
          changefreq: string;
        }[]
      | {
          loc: string;
          'news:news': {
            'news:publication': [];
            'news:keywords': string;
            'news:publication_date': string;
            'news:title': string;
          };
        }[];
  };
}
