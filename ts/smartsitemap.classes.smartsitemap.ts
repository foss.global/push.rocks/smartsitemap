import { SitemapNews } from './smartsitemap.classes.sitemapnews.js';
import { IUrlInfo, SitemapWebsite } from './smartsitemap.classes.sitemapwebsite.js';
import * as plugins from './smartsitemap.plugins.js';
import * as interfaces from './interfaces/index.js';

export class SmartSitemap {
  constructor() {}

  /**
   * creates a sitemap for news from feedurl
   */
  public async createSitemapNewsFromFeedUrl(feedUrlArg: string): Promise<string> {
    const sitemapNewsInstance = new SitemapNews({});
    await sitemapNewsInstance.readAndAddFromRssFeedUrl(feedUrlArg);
    return sitemapNewsInstance.exportSitemapXml();
  }

  /**
   * creates a sitemap for news from feedxmlstring
   */
  public async createSitemapNewsFromAFeedStringArg(feedStringArg: string): Promise<string> {
    const sitemapNewsInstance = new SitemapNews({});
    await sitemapNewsInstance.readAndAddFromRssFeedString(feedStringArg);
    return sitemapNewsInstance.exportSitemapXml();
  }

  /**
   * creates a sitemap for news from an array of articles
   */
  public async createSitemapNewsFromArticleArray(articleArrayArg: plugins.tsclass.content.IArticle[]): Promise<string> {
    const sitemapNewsInstance = new SitemapNews({});
    await sitemapNewsInstance.readAndParseArticles(articleArrayArg);
    return sitemapNewsInstance.exportSitemapXml();
  }

  /**
   * creates a normal sitemap from a list of urls
   */
  public async createSitemapFromYmlString(yamlString: string): Promise<string> {
    const yamlObject: interfaces.ISitemapYaml = await plugins.smartyaml.yamlStringToObject(yamlString);
    const sitemapWebsite = new SitemapWebsite();
    for(const urlArg of yamlObject.daily) {
      sitemapWebsite.addUrl({
        url: urlArg,
        timestamp: Date.now() - 10000,
        frequency: 'daily'
      });
    }
    return sitemapWebsite.exportSitemapXml();
  }

  /**
   * creates a normal sitemap from a list of urls
   */
  public async createSitemapFromUrlInfoArray(urlInfosArg: IUrlInfo[]) {
    const sitemapWebsite = new SitemapWebsite();
    for(const urlInfo of urlInfosArg) {
      sitemapWebsite.addUrl(urlInfo);
    }
    return sitemapWebsite.exportSitemapXml();
  }

  /**
   * parses a sitemap url
   */
  public async parseSitemapUrl(urlArg: string) {
    const sitemapXml = await (await (new plugins.webrequest.WebRequest()).request(urlArg, {
      method: 'GET'
    })).text();

    const parsedSitemap = await this.parseSitemap(sitemapXml);
    return parsedSitemap;
  }

  /**
   * parses a sitemap
   */
  public async parseSitemap(sitemapXmlArg: string): Promise<interfaces.IParsedSiteMap> {
    return (new plugins.smartxml.SmartXml()).parseXmlToObject(sitemapXmlArg);
  }
}
